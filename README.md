## How to Play:

1. Collect as many coins as you can and get to the flagpole, while trying not to die in the process (not easy).
2. If a daemon (pun intended) gets within 200 units of your location, it will spot you and give chase.
3. If it catches you, you will lose 100 points, and in exchange, the daemon will be killed off.
4. However, if you don't have enough points to kill the daemon, it is you who must die.

Tip: Explore both sides to get all the coins!

Have fun!