/*

The Game Project 7 - Final Version

How to Play:
1. Collect as many coins as you can and get to the flagpole, while trying not to die in the process (not easy).
2. If a daemon (pun intended) gets within 200 units of your location, it will spot you and give chase.
3. If it catches you, you will lose 100 points, and in exchange, the daemon will be killed off.
4. However, if you don't have enough points to kill the daemon, it is you who must die.

Tip: Explore both sides to get all the coins!

Have fun!

*/


/*
Extensions added:

1. Platforms
2. Enemies

For platforms, I tried to implement a bounding-box collision model to give a more realistic behaviour
when the character touches the platform from any side, not just from above. Also, I enhanced the jump function to
allow for a smooth rise up in the air, rather than a sudden teleportation 100 units up. This allows for a neat
demonstration of what happens when the character hits a platform from below. The bounding box implementation was
quite interesting to implement. What I found challenging was to get it to work with tolerances, since between two
consecutive frames, the character's box and the platform's box can go from no contact to non-zero overlap, since
the character's position changes more than 1 pixel each frame. This meant I could not use equality of box boundary
positions to check for the exact moment of collision. Some tolerance for overlap had to be built in. After some trial
and error, I found that a tolerance value equal to the character's moving velocity worked reasonably well. This is
probably because the character cannot move more than that amount between two successive frames.

For the enemies, I decided to build in two modes - one being a random float-about through the game world, and the
other being a deliberate pursuit mode when the character has been sighted. They give a visual cue when in pursuit
to give the player a hint that they are being chased. The challenging part was to keep the enemies from wandering
too far off the playable canvas while in random walk mode - they would never spot the player if they did, and thus
would be useless to the gameplay. To achieve this, I used the already existing `scrollPos` variable to constrain
their translated world positions to within a range of the window width and height. Of course, when in pursuit mode,
they will just follow the player until they catch up, or until the player outruns them.

My overall focus of development in this game has been on implementing believable physics (movement and collisions),
and engaging and challenging gameplay using strategic platform placements, coins with a mind of their own, and
daemons that are not to be trifled with. As a trade-off, I have kept the graphics quite bareboned, with just enough
visual hints to the player that aid in gameplay. As for sound, there is a background track that might lend some tense
ambience to the game. However, no event-based sound-effects were added, though that would have been good to implement,
had I had enough time.
*/

var gameChar_x;
var gameChar_y;
var floorPos_y;
var scrollPos;
var gameChar_world_x;
var game_score;
var lives;
var gameTime;
var muzak;

var isLeft;
var isRight;
var isFalling;
var isRising;
var riseTill;
var isPlummeting;
var leftIsFree;
var rightIsFree;
var topIsFree;
var botIsFree;

var trees_x;
var treePos_y;
var clouds;
var mountains;
var canyons;
var collectables;
var flagpole;
var platforms;
var enemies;

function preload() {
  soundFormats('mp3');
  muzak = loadSound('Hall of the Mountain King.mp3');
}

function setup() {
	createCanvas(windowWidth, windowHeight);
	lives = 3;
	floorPos_y = height * 3 / 4;
	angleMode(DEGREES);
	muzak.setVolume(1.0);
	muzak.loop();

	startGame();
}

function startGame() {
	gameTime = 0;

	gameChar_x = 500;
	gameChar_y = floorPos_y;

	// Variable to control the background scrolling.
	scrollPos = 0;

	// Variable to store the real position of the gameChar in the game
	// world. Needed for collision detection.
	gameChar_world_x = gameChar_x - scrollPos;

	// Boolean variables to control the movement of the game character.
	isLeft = false;
	isRight = false;
	isFalling = false;
	isRising = false;
	isPlummeting = false;

	//Initialize game score.
	game_score = 0;

	// Initialise arrays of scenery objects.
	treePos_y = floorPos_y - 146;
	trees_x = [-700, -300, -50, 320, 1000, 1500, 1900];

	clouds = [{
		x_pos: -1100,
		y_pos: 200,
		size: 100
	}, {
		x_pos: -500,
		y_pos: 150,
		size: 80
	}, {
		x_pos: -100,
		y_pos: 50,
		size: 40
	}, {
		x_pos: 200,
		y_pos: 100,
		size: 80
	}, {
		x_pos: 500,
		y_pos: 80,
		size: 40
	}, {
		x_pos: 1000,
		y_pos: 150,
		size: 60
	}, {
		x_pos: 1300,
		y_pos: 100,
		size: 40
	}, {
		x_pos: 1800,
		y_pos: 100,
		size: 80
	}, {
		x_pos: 2300,
		y_pos: 50,
		size: 40
	}];

	mountains = [{
		x_pos: -900,
		y_pos: floorPos_y,
		width: 200,
		height: 100
	}, {
		x_pos: -350,
		y_pos: floorPos_y,
		width: 300,
		height: 200
	}, {
		x_pos: 400,
		y_pos: floorPos_y,
		width: 100,
		height: 50
	}, {
		x_pos: 900,
		y_pos: floorPos_y,
		width: 400,
		height: 300
	}, {
		x_pos: 1500,
		y_pos: floorPos_y,
		width: 500,
		height: 300
	}];

	canyons = [{
		x_pos: -2000,
		width: 1000
	}, {
		x_pos: 0,
		width: 300
	}, {
		x_pos: 550,
		width: 350
	}, {
		x_pos: 1350,
		width: 100
	}, {
		x_pos: 2000,
		width: 300
	}, {
		x_pos: 2500,
		width: 175
	}];

	collectables = [{
		x_pos_base: -1190,
		size: 50,
		isTricky_y: 1,
		isTricky_x: 1
	}, {
		x_pos: -350,
		y_pos: floorPos_y - 15,
		size: 30,
		isWicked: true
	}, {
		x_pos_base: 250,
		size: 50,
		isTricky_x_2: random([-1, 1]),
		isTricky_y: 1
	}, {
		x_pos: 380,
		y_pos: floorPos_y - 15,
		size: 30
	}, {
		x_pos: 450,
		y_pos: floorPos_y - 250,
		size: 50
	}, {
		x_pos_base: 750,
		y_pos: floorPos_y - 150,
		size: 50,
		isTricky_x: 1
	}, {
		x_pos: 1200,
		y_pos: floorPos_y - 15,
		size: 30,
		isWicked: true
	}, {
		x_pos: 1400,
		size: 50,
		isTricky_y: 1
	}, {
		x_pos: 1800,
		y_pos: floorPos_y - 15,
		size: 30,
		isWicked: true
	}, {
		x_pos: 2600,
		size: 50,
		isTricky_y: 1
	}, {
		x_pos: 2550,
		size: 50,
		isTricky_y: -1
	}];

	flagpole = {
		x_pos: 2700,
		isReached: false
	}

	platforms = [];
	platforms.push(createPlatform(-1100, floorPos_y - 50, 50, 20));
	platforms.push(createPlatform(125, floorPos_y - 50, 50, 20));
	platforms.push(createPlatform(400, floorPos_y - 120, 100, 20));
	platforms.push(createPlatform(690, floorPos_y - 70, 50, 20));
	platforms.push(createPlatform(2125, floorPos_y - 10, 50, 20));

	enemies = [];
	enemies.push(new Enemy(-1200, floorPos_y - 200));
	enemies.push(new Enemy(-400, floorPos_y - 200));
	enemies.push(new Enemy(0, floorPos_y - 200));
	enemies.push(new Enemy(800, floorPos_y - 200));
	enemies.push(new Enemy(1600, floorPos_y - 200));
	enemies.push(new Enemy(2000, floorPos_y - 200));
	enemies.push(new Enemy(2400, floorPos_y - 200));
	enemies.push(new Enemy(2800, floorPos_y - 200));
}

function draw() {
	gameTime++;

	background(100, 155, 255); // fill the sky blue

	noStroke();
	fill(0, 155, 0);
	rect(0, floorPos_y, width, height / 4); // draw some green ground

	push();
	translate(scrollPos, 0);

	// Draw clouds.
	drawClouds();

	// Draw mountains.
	drawMountains();

	// Draw trees.
	drawTrees();

	// Draw canyons.
	for (var i = 0; i < canyons.length; i++) {
		drawCanyon(canyons[i]);
	}

	//Draw platforms.
	for (var i = 0; i < platforms.length; i++) {
		platforms[i].draw();
	}

	// Draw collectable items.
	for (var i = 0; i < collectables.length; i++) {
		if (!collectables[i].isFound) {
			drawCollectable(collectables[i]);
			checkCollectable(collectables[i]);
		}
	}

	//Draw enemies
	var killOff = [];
	for (var i = 0; i < enemies.length; i++) {
		enemies[i].draw();
		if (!flagpole.isReached && enemies[i].checkContact()) {
			killOff.push(i);
			game_score -= 100;
		}
	}

	if (game_score < 0) {
		isPlummeting = true;
	} else {
		for (var i = 0; i < killOff.length; i++) {
			enemies.splice(killOff[i], 1);
		}
	}

	//Draw the flagpole.
	renderFlagpole();

	//Some helpful in-game text to help the player navigate.
	textSize(20);
	fill(0, 0, 255);
	if (!flagpole.isReached) {
		text('Take a leap of faith ==>', 2310, height - 20);
	}
	text('| Edge of the world', -1000, height - 20);

	fill(255);
	text('<== Explore both ways ==>', 300, height - 20);

	pop();

	// Draw game character.
	drawGameChar();

	//Render score and lives.
	push();
	textSize(20);
	fill(255);
	text(`Score: ${game_score}/${collectables.reduce((total, c) => total + c.size, 0)}`, 10, 22);
	textSize(30);
	fill(255, 0, 0);
	text('♥'.repeat(Math.max(lives, 0)), 560, 32);
	pop();

	//Render end-of-game messages, when required.
	if (lives < 1) {
		push();
		textSize(40);
		fill(255, 0, 0);
		text('Game over. Press space to continue.', width / 2 - 370, height / 2);
		pop();
	} else if (flagpole.isReached) {
		push();
		textSize(40);
		fill(0, 255, 0);
		text('Level complete. Press space to continue.', width / 2 - 410, height / 2);
		pop();
	}

	//Check for platform contact using bounding box collision.
	var isContact = false,
		isContactAbove = false,
		isContactBelow = false,
		isContactLeft = false,
		isContactRight = false;

	for (var i = 0; i < platforms.length; i++) {
		var ct = platforms[i].checkContact(gameChar_world_x, gameChar_y);

		if (ct.isContact) {
			isContact = ct.isContact; //true
			isContactAbove = ct.isAbove;
			isContactBelow = ct.isBelow;
			isContactLeft = ct.isLeft;
			isContactRight = ct.isRight;

			break;
		}
	}

	//Check which sides are available for the character to freely move towards.
	leftIsFree = !isContact || !isContactRight || isContactAbove || isContactBelow;
	rightIsFree = !isContact || !isContactLeft || isContactAbove || isContactBelow;
	topIsFree = !isContact || !isContactBelow || isContactLeft || isContactRight;
	botIsFree = !isContact || !isContactAbove || isContactLeft || isContactRight;

	// Logic to make the game character move or the background scroll.
	if (isLeft && !isPlummeting && !flagpole.isReached && leftIsFree) {
		if (gameChar_x > width * 0.2) {
			gameChar_x -= 5;
		} else {
			scrollPos += 5;
		}
	}

	if (isRight && !isPlummeting && !flagpole.isReached && rightIsFree) {
		if (gameChar_x < width * 0.8) {
			gameChar_x += 5;
		} else {
			scrollPos -= 5;
		}
	}

	// Logic to make the game character rise and fall.
	if (isPlummeting) { //Youse a dead duck!
		gameChar_y += 6;
	} else {
		if (!isRising && (gameChar_y < floorPos_y) && botIsFree) {
			isFalling = true;
		} else {
			isFalling = false;
		}

		if (isRising && gameChar_y > riseTill && topIsFree) {
			gameChar_y -= 6;
		} else {
			isRising = false;
		}

		if (isFalling) {
			gameChar_y += 3;
		}
	}

	// Update real position of gameChar for collision detection.
	gameChar_world_x = gameChar_x - scrollPos;

	for (var i = 0; i < canyons.length; i++) {
		checkCanyon(canyons[i]);
		if (isPlummeting) {
			break;
		}
	}

	//Did he die dead enough??
	checkPlayerDie();
}

// ---------------------
// Key control functions
// ---------------------

function keyPressed() {
	if (lives > 0) {
		if (keyCode == 37) {
			isLeft = true;
		} else if (keyCode == 39) {
			isRight = true;
		} else if ((keyCode == 32) && !isFalling && !flagpole.isReached && !isRising && !isPlummeting) {
			isRising = true;
			riseTill = gameChar_y - 100;
		} else if ((keyCode == 32) && flagpole.isReached) {
			lives = 3;
			startGame();
		}
	} else if (keyCode == 32) {
		lives = 3;
		startGame();
	}
}

function keyReleased() {
	if (keyCode == 37) {
		isLeft = false;
	} else if (keyCode == 39) {
		isRight = false;
	}
}

// ------------------------------
// Game character render function
// ------------------------------

// Function to draw the game character.

function drawGameChar() {
	// draw game character
	if (isLeft && (isFalling || isPlummeting || isRising)) {
		// add your jumping-left code
		fill(255, 160, 122);
		ellipse(gameChar_x, gameChar_y - 60, 30);
		triangle(gameChar_x - 10, gameChar_y - 70, gameChar_x - 25, gameChar_y - 60, gameChar_x - 15, gameChar_y - 60);
		push()
		fill(0);
		stroke(255);
		strokeWeight(3);
		ellipse(gameChar_x - 10, gameChar_y - 65, 5, 5);
		pop();
		fill(255, 0, 0);
		rect(gameChar_x - 10, gameChar_y - 45, 20, 30);
		fill(0);
		rect(gameChar_x - 24, gameChar_y - 20, 10, 8);
		rect(gameChar_x + 15, gameChar_y - 20, 10, 8);
	} else if (isRight && (isFalling || isPlummeting || isRising)) {
		// add your jumping-right code
		fill(255, 160, 122);
		ellipse(gameChar_x, gameChar_y - 60, 30);
		triangle(gameChar_x + 10, gameChar_y - 70, gameChar_x + 25, gameChar_y - 60, gameChar_x + 15, gameChar_y - 60);
		push()
		fill(0);
		stroke(255);
		strokeWeight(3);
		ellipse(gameChar_x + 10, gameChar_y - 65, 5, 5);
		pop();
		fill(255, 0, 0);
		rect(gameChar_x - 10, gameChar_y - 45, 20, 30);
		fill(0);
		rect(gameChar_x - 24, gameChar_y - 20, 10, 8);
		rect(gameChar_x + 15, gameChar_y - 20, 10, 8);
	} else if (isLeft) {
		// add your walking left code
		fill(255, 160, 122);
		ellipse(gameChar_x, gameChar_y - 60, 30);
		triangle(gameChar_x - 10, gameChar_y - 70, gameChar_x - 25, gameChar_y - 60, gameChar_x - 15, gameChar_y - 60);
		push()
		fill(0);
		stroke(255);
		strokeWeight(3);
		ellipse(gameChar_x - 10, gameChar_y - 65, 5, 5);
		pop();
		fill(255, 0, 0);
		rect(gameChar_x - 10, gameChar_y - 45, 20, 40);
		fill(0);
		rect(gameChar_x - 20, gameChar_y - 5, 10, 8);
		rect(gameChar_x + 10, gameChar_y - 5, 10, 8);
	} else if (isRight) {
		// add your walking right code
		fill(255, 160, 122);
		ellipse(gameChar_x, gameChar_y - 60, 30);
		triangle(gameChar_x + 10, gameChar_y - 70, gameChar_x + 25, gameChar_y - 60, gameChar_x + 15, gameChar_y - 60);
		push()
		fill(0);
		stroke(255);
		strokeWeight(3);
		ellipse(gameChar_x + 10, gameChar_y - 65, 5, 5);
		pop();
		fill(255, 0, 0);
		rect(gameChar_x - 10, gameChar_y - 45, 20, 40);
		fill(0);
		rect(gameChar_x - 20, gameChar_y - 5, 10, 8);
		rect(gameChar_x + 10, gameChar_y - 5, 10, 8);
	} else if (isFalling || isPlummeting || isRising) {
		// add your jumping facing forwards code
		fill(255, 160, 122);
		ellipse(gameChar_x, gameChar_y - 60, 30);
		push()
		fill(0);
		stroke(255);
		strokeWeight(3);
		ellipse(gameChar_x - 5, gameChar_y - 65, 5, 5);
		ellipse(gameChar_x + 5, gameChar_y - 65, 5, 5);
		pop();
		fill(255, 0, 0);
		rect(gameChar_x - 15, gameChar_y - 45, 30, 30);
		fill(0);
		rect(gameChar_x - 24, gameChar_y - 20, 10, 8);
		rect(gameChar_x + 15, gameChar_y - 20, 10, 8);
	} else {
		// add your standing front facing code
		fill(255, 160, 122);
		ellipse(gameChar_x, gameChar_y - 60, 30);
		push()
		fill(0);
		stroke(255);
		strokeWeight(3);
		ellipse(gameChar_x - 5, gameChar_y - 65, 5, 5);
		ellipse(gameChar_x + 5, gameChar_y - 65, 5, 5);
		pop();
		fill(255, 0, 0);
		rect(gameChar_x - 15, gameChar_y - 45, 30, 40);
		fill(0);
		rect(gameChar_x - 20, gameChar_y - 5, 10, 8);
		rect(gameChar_x + 10, gameChar_y - 5, 10, 8);
	}

	if (isPlummeting) { //Oh the horror!!
		fill(255, 0, 0);
		ellipse(gameChar_x, gameChar_y - 50, 10);
	}
}

// ---------------------------
// Background render functions
// ---------------------------

// Function to draw cloud objects.
function drawClouds() {
	for (var i = 0; i < clouds.length; i++) {
		fill(255);
		ellipse(clouds[i].x_pos, clouds[i].y_pos, clouds[i].size);
		ellipse(clouds[i].x_pos - clouds[i].size / 2, clouds[i].y_pos, clouds[i].size * 2 / 3);
		ellipse(clouds[i].x_pos + clouds[i].size / 2, clouds[i].y_pos, clouds[i].size * 2 / 3);
	}
}

// Function to draw mountains objects.
function drawMountains() {
	for (var i = 0; i < mountains.length; i++) {
		fill(175);
		triangle(mountains[i].x_pos + mountains[i].width / 3, mountains[i].y_pos - mountains[i].height,
			mountains[i].x_pos, mountains[i].y_pos,
			mountains[i].x_pos + mountains[i].width * 2 / 3, mountains[i].y_pos);
		fill(255);
		triangle(mountains[i].x_pos + mountains[i].width / 3, mountains[i].y_pos - mountains[i].height,
			mountains[i].x_pos + mountains[i].width / 4, mountains[i].y_pos - mountains[i].height * 3 / 4,
			mountains[i].x_pos + mountains[i].width * 5 / 12, mountains[i].y_pos - mountains[i].height * 3 / 4);
		fill(175);
		triangle(mountains[i].x_pos + mountains[i].width * 2 / 3, mountains[i].y_pos - mountains[i].height * 2 / 3,
			mountains[i].x_pos + mountains[i].width / 3, mountains[i].y_pos,
			mountains[i].x_pos + mountains[i].width, mountains[i].y_pos);
		fill(255);
		triangle(mountains[i].x_pos + mountains[i].width * 2 / 3, mountains[i].y_pos - mountains[i].height * 2 / 3,
			mountains[i].x_pos + mountains[i].width * 7 / 12, mountains[i].y_pos - mountains[i].height / 2,
			mountains[i].x_pos + mountains[i].width * 3 / 4, mountains[i].y_pos - mountains[i].height / 2);
	}
}

// Function to draw trees objects.
function drawTrees() {
	for (var i = 0; i < trees_x.length; i++) {
		fill(163, 113, 60);
		rect(trees_x[i], treePos_y + 60, 20, 86);
		fill(0, 155, 0);
		triangle(trees_x[i] + 10, treePos_y + 36, trees_x[i] - 40, treePos_y + 80, trees_x[i] + 60, treePos_y + 80);
		triangle(trees_x[i] + 10, treePos_y + 4, trees_x[i] - 40, treePos_y + 50, trees_x[i] + 60, treePos_y + 50);
	}
}

// ---------------------------------
// Canyon render and check functions
// ---------------------------------

// Function to draw canyon objects.
function drawCanyon(t_canyon) {
	fill(100, 155, 255);
	rect(t_canyon.x_pos, floorPos_y, t_canyon.width, floorPos_y);
}

// Function to check character is over a canyon.
function checkCanyon(t_canyon) {
	if ((t_canyon.x_pos + 15 < gameChar_world_x) && (t_canyon.x_pos + t_canyon.width - 15 > gameChar_world_x) && (gameChar_y >= floorPos_y)) {
		isPlummeting = true;
		isLeft = false;
		isRight = false;
	}
}

// ----------------------------------
// Collectable items render and check functions
// ----------------------------------

// Function to draw collectable objects.
function drawCollectable(t_collectable) {
	//Check is the collectable is static or moving and update its coordinates accordingly.
	if (t_collectable.isTricky_x) {
		t_collectable.x_pos = t_collectable.x_pos_base + t_collectable.isTricky_x * height * cos(2 * gameTime) / 4;
	} else if (t_collectable.isTricky_x_2) {
		t_collectable.x_pos = t_collectable.x_pos_base + 2 * gameTime * t_collectable.isTricky_x_2;
	} else if (t_collectable.isWicked && (abs(gameChar_world_x - t_collectable.x_pos) < 40) && (gameChar_y >= floorPos_y)) {
		t_collectable.x_pos = gameChar_world_x - 40 * Math.sign(gameChar_world_x - t_collectable.x_pos);
	}

	if (t_collectable.isTricky_y) {
		t_collectable.y_pos = floorPos_y + t_collectable.isTricky_y * height * sin(2 * gameTime) / 4;
	}

	//Draw the collectable.
	fill(255, 255, 0);
	ellipse(t_collectable.x_pos, t_collectable.y_pos, t_collectable.size);
	fill(240, 200, 0);
	ellipse(t_collectable.x_pos, t_collectable.y_pos, t_collectable.size - 10);
	push();
	stroke(1);
	text("$$", t_collectable.x_pos - 7, t_collectable.y_pos + 4);
	if (t_collectable.isWicked && (abs(gameChar_world_x - t_collectable.x_pos) < 50)) {
		fill(255, 0, 255);
		noStroke();

		var hint_x_pos;
		if (gameChar_world_x < t_collectable.x_pos) {
			hint_x_pos = t_collectable.x_pos + 20;
		} else {
			hint_x_pos = t_collectable.x_pos - 130;
		}
		text("Pounce to collect!", hint_x_pos, t_collectable.y_pos - 20);
	}
	pop();
}

// Function to check character has collected an item.
function checkCollectable(t_collectable) {
	if (dist(gameChar_world_x, gameChar_y - 35, t_collectable.x_pos, t_collectable.y_pos) < 35) {
		t_collectable.isFound = true;
		game_score += t_collectable.size;
	}
}

//Draws the flagpole
function renderFlagpole() {
	if (abs(gameChar_world_x - flagpole.x_pos) < 10) {
		flagpole.isReached = true;
	}

	push();
	strokeWeight(5);
	stroke(180);
	line(flagpole.x_pos, floorPos_y, flagpole.x_pos, floorPos_y - 250);

	var flagHeight = flagpole.isReached ? floorPos_y - 200 : floorPos_y;
	noStroke();
	fill(255, 204, 0);
	triangle(flagpole.x_pos, flagHeight, flagpole.x_pos - 50, flagHeight, flagpole.x_pos, flagHeight - 50);
	pop();
}

//Checks if the character is dead, and if he has lives left for a revival.
function checkPlayerDie() {
	if (gameChar_y > height) {
		lives--;

		if (lives > 0) {
			startGame();
		}
	}
}

//Factory function to create platforms
function createPlatform(x, y, length, height) {
	var p = {
		x: x,
		y: y,
		length: length,
		height: height,
		draw: function () {
			fill(178, 34, 34);
			rect(this.x, this.y, this.length, this.height);
		},
		checkContact: function (gc_x, gc_y) {
			//Define character's bounding box.
			var gc_left = gc_x - 15;
			var gc_right = gc_x + 15;
			var gc_top = gc_y - 75;
			var gc_bot = gc_y;

			//Define platform's bounding box.
			var left = this.x;
			var right = this.x + this.length;
			var top = this.y;
			var bot = this.y + this.height;

			var isContact = false,
				isAbove = false,
				isBelow = false,
				isLeft = false,
				isRight = false;

			//Bounding box collision implementation.
			if ((gc_left <= right) && (gc_right >= left) && (gc_top <= bot) && (gc_bot >= top)) { //Contact
				isContact = true;
				isAbove = gc_bot <= top + 6; //GC is above platform
				isBelow = gc_top >= bot - 6; //GC is below platform
				isLeft = gc_right <= left + 6; //GC is to the left of platform
				isRight = gc_left >= right - 6; //GC is to the right of platform
			}

			return {
				isContact,
				isAbove,
				isBelow,
				isLeft,
				isRight
			};
		}
	};

	return p;
}

//Constructor function for creating enemies
function Enemy(x, y) {
	this.speed = 1;
	this.currentX = x;
	this.currentY = y;
	this.speedX = this.speed * random([-1, 1]);
	this.speedY = this.speed * random([-1, 1]);
	this.inPursuit = false;

	this.update = function () {
		this.inPursuit = dist(this.currentX, this.currentY, gameChar_world_x, gameChar_y - 37) < 200;

		if (!this.inPursuit) {
			if (gameTime % 100 === 0) {
				this.speedX = this.speed * random([-1, 1]);
				this.speedY = this.speed * random([-1, 1]);
			}

			var screenX = this.currentX + scrollPos;

			if (screenX < 0 || screenX > width) {
				this.speedX = -this.speedX;
			}

			if (this.currentY < 0 || this.currentY > height) {
				this.speedY = -this.speedY;
			}
		} else { //I see you!
			this.speedX = 1.5 * this.speed * Math.sign(gameChar_world_x - this.currentX);
			this.speedY = 1.5 * this.speed * Math.sign(gameChar_y - 37 - this.currentY);
		}

		this.currentX += this.speedX;
		this.currentY += this.speedY;

	}

	this.draw = function () {
		this.update();

		push();
		fill(0);
		ellipse(this.currentX, this.currentY, 50);
		strokeWeight(3);
		stroke(255);
		line(this.currentX - 10, this.currentY - 15, this.currentX - 2, this.currentY - 12);
		line(this.currentX + 10, this.currentY - 15, this.currentX + 2, this.currentY - 12);
		line(this.currentX, this.currentY, this.currentX - 15, this.currentY + 5);
		line(this.currentX, this.currentY, this.currentX + 15, this.currentY + 5);
		pop();

		if (this.inPursuit) { //Scary mode!
			push();
			strokeWeight(2);
			stroke(255);
		}
		fill(255, 0, 0);
		ellipse(this.currentX - 8, this.currentY - 10, 7, 3);
		ellipse(this.currentX + 8, this.currentY - 10, 7, 3);
		triangle(this.currentX - 12, this.currentY + 10, this.currentX + 12, this.currentY + 10, this.currentX, this.currentY + 2);
		if (this.inPursuit) {
			pop();
		}
	}

	this.checkContact = function () {
		return dist(this.currentX, this.currentY, gameChar_world_x, gameChar_y - 35) < 35;
	}
}